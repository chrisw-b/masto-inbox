import { checkAuthState, processAuthorizationCode } from "~/utils/auth";

const { search } =
  typeof window.location === "string"
    ? new URL(window.location)
    : window.location;
const params = new URLSearchParams(search);

const login = async () => {
  const authState = await checkAuthState();
  if (!authState && params.has("code")) {
    try {
      await processAuthorizationCode(params.get("code") ?? "");
      window.location.replace("/");
    } catch (e) {
      console.error("Getting code from query failed due to ", e);
    }
  } else if (!authState) {
    window.location.replace("/login");
  }
};

login().catch(console.error);
