import {
  createRestAPIClient,
  createStreamingAPIClient,
  type mastodon,
} from "masto";
import { localStore } from "./store";

type AuthState = {
  loggedIn: boolean;
  accessToken?: string | undefined;
  account?: mastodon.v1.Account | undefined;
  instance?: mastodon.v1.Instance | undefined;
};

export let apiClient: mastodon.rest.Client | null = null;
export let streamingClient: mastodon.streaming.Client | null = null;
const authStore: AuthState = { loggedIn: false };

const {
  PUBLIC_APPLICATION_NAME: APPLICATION_NAME,
  PUBLIC_APPLICATION_WEBSITE: APPLICATION_WEBSITE,
} = import.meta.env;

export type OAuthState = {
  instanceHost: string;
  client_id: string;
  client_secret: string;
  access_token?: string | undefined;
};

export const prepareLoginURL = async (instanceHost: string) => {
  let oauthState = localStore.get<OAuthState>("oauth");
  if (oauthState == null || oauthState.instanceHost !== instanceHost) {
    const { client_id, client_secret } =
      await registerApplication(instanceHost);
    oauthState = { instanceHost, client_id, client_secret };

    localStore.set<OAuthState>("oauth", oauthState);
  }

  return getAuthorizationURL(instanceHost, oauthState.client_id);
};

const getAuthorizationURL = (instanceHost: string, client_id: string) => {
  const authorizationParams = new URLSearchParams({
    client_id,
    scope: "read write follow",
    redirect_uri: location.origin,
    response_type: "code",
  });

  return `https://${instanceHost}/oauth/authorize?${authorizationParams.toString()}`;
};

const registerApplication = async (instanceHost: string) => {
  const registrationParams = new URLSearchParams({
    client_name: APPLICATION_NAME,
    redirect_uris: location.origin,
    scopes: "read write follow",
    website: APPLICATION_WEBSITE,
  });

  const registrationResponse = await fetch(
    `https://${instanceHost}/api/v1/apps`,
    {
      method: "POST",
      headers: { "Content-Type": "application/x-www-form-urlencoded" },
      body: registrationParams.toString(),
    },
  );

  const registrationJSON = await registrationResponse.json();
  return registrationJSON;
};

export const checkAuthState = async () => {
  const oauthState = localStore.get<OAuthState>("oauth");
  if (oauthState == null) {
    return false;
  }

  const { access_token, instanceHost } = oauthState;
  if (!access_token) {
    return false;
  }

  try {
    await initClient(instanceHost, access_token);
  } catch (e) {
    return false;
  }
  return true;
};

const initClient = async (instanceHost: string, access_token: string) => {
  const client = createRestAPIClient({
    url: `https://${instanceHost}`,
    accessToken: access_token,
    timeout: 30_000,
  });

  try {
    const user = await client.v1.accounts.verifyCredentials();
    const instance = await client.v1.instance.fetch();

    apiClient = client;
    try {
      streamingClient = createStreamingAPIClient({
        streamingApiUrl: instance.urls.streamingApi,
        accessToken: access_token,
        implementation: window.WebSocket,
        retry: 1,
      });
    } catch (e) {
      console.error("Connecting to Streaming API failed");
    }

    authStore.loggedIn = true;
    authStore.accessToken = access_token;

    authStore.account = user;
    authStore.instance = instance;
  } catch (e) {
    const oauthState = localStore.get<OAuthState>("oauth");
    if (!oauthState) {
      throw new Error("Invalid access token");
    }

    // TODO: Show error, redirect to login
    localStore.set<OAuthState>("oauth", {
      ...oauthState,
      access_token: undefined,
    });
    throw new Error("Invalid access token");
  }
};

export const processAuthorizationCode = async (code: string) => {
  const oauthState = localStore.get<OAuthState>("oauth");
  if (oauthState == null) {
    throw new Error("No OAuth state found");
  }

  const { client_id, client_secret, instanceHost } = oauthState;

  const { access_token } = await getAccessToken(
    instanceHost,
    client_id,
    client_secret,
    code,
  );

  if (!access_token) {
    throw new Error("Invalid code");
  }

  localStore.set<OAuthState>("oauth", { ...oauthState, access_token });

  try {
    await initClient(instanceHost, access_token);
  } catch (e) {
    throw new Error("Failed to access user profile");
  }
};

const getAccessToken = async (
  instanceHost: string,
  client_id: string,
  client_secret: string,
  code: string,
) => {
  const params = new URLSearchParams({
    client_id,
    client_secret,
    code,
    redirect_uri: location.origin,
    grant_type: "authorization_code",
    scope: "read write follow",
  });

  const tokenResponse = await fetch(`https://${instanceHost}/oauth/token`, {
    method: "POST",
    headers: { "Content-Type": "application/x-www-form-urlencoded" },
    body: params.toString(),
  });
  if (tokenResponse.ok) {
    const tokenJSON = await tokenResponse.json();
    return tokenJSON;
  }
  throw new Error(
    `Error getting access token: ${tokenResponse.status} / ${tokenResponse.statusText}`,
  );
};
