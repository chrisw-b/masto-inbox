type Store = {
  get: <T>(key: string) => T | null;
  set: <T>(key: string, value: T) => void;
};

/**
 * Simple persistent key (string) -> value (json object) store
 */
const storeBase = (storage: Storage): Store => {
  return {
    get: <T>(key: string): T | null => {
      const value = storage.getItem(key);
      if (value === null) {
        return null;
      }
      return JSON.parse(value) as T;
    },

    set: <T>(key: string, value: T) => {
      if (!value) {
        return storage.removeItem(key);
      }
      storage.setItem(key, JSON.stringify(value));
    },
  };
};

export const sessionStore: Store = storeBase(sessionStorage);
export const localStore: Store = storeBase(localStorage);
